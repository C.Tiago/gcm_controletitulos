/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import dao.DaoTitulo;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.List;
import org.jdesktop.observablecollections.ObservableCollections;
import domain.Titulo;
/**
 *
 * @author Gerim
 */
public class TituloController {
    
    private final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);  
    private Titulo tituloDigitado;
    private Titulo tituloSelecionado;
    private List<Titulo> titulosTabelas;
    private final DaoTitulo titulodao;

    public Titulo getTituloDigitado() {
        return tituloDigitado;
    }

    public void setTituloDigitado(Titulo tituloDigitado) {
        Titulo oldtituloDigitado = this.tituloDigitado;
        this.tituloDigitado = tituloDigitado;
        propertyChangeSupport.firePropertyChange
        ("tituloDigitado",oldtituloDigitado,tituloDigitado); 
    }

    public Titulo getTituloSelecionado() {
        return tituloSelecionado;
    }

    public void settituloSelecionado(Titulo tituloSelecionado) {
        if(this.tituloSelecionado !=null){
            setTituloDigitado(tituloSelecionado);
        }
        
        this.tituloSelecionado = tituloSelecionado;
    }

    public List<Titulo> gettitulosTabelas() {
        return titulosTabelas;
    }

    public void setTitulosTabelas(List<Titulo> titulosTabelas) {
        this.titulosTabelas = titulosTabelas;
    }
    
    public TituloController(){
        titulodao = new DaoTitulo();
        titulosTabelas = ObservableCollections.observableList(new ArrayList<Titulo>());
        novo();
        pesquisar();     
    }
    
    public void salvar(){
        titulodao.salvarAtualizar(tituloDigitado);
        novo();
        pesquisar();
    }
    
    public void excluir(){
        titulodao.excluir(tituloDigitado);
        novo();
        pesquisar();
    }
    
    public void novo() {
        setTituloDigitado(new Titulo());
        
    }

    public void pesquisar() {
        titulosTabelas.clear();
        titulosTabelas.addAll(titulodao.pesquisar(tituloDigitado));
    }
    
    public void addPropertyChangeListener(PropertyChangeListener e) {
        propertyChangeSupport.addPropertyChangeListener(e);
    }
    
    public void removePropertyChangeListener(PropertyChangeListener e) {
        propertyChangeSupport.removePropertyChangeListener(e);
    }

    public void setTituloController(Titulo c) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
