/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import dao.DaoClube;
import domain.Clube;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.List;
import org.jdesktop.observablecollections.ObservableCollections;
/**
 *
 * @author C.Tiago
 */
public final class ClubeController {
    
    private final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);  
    private Clube clubeDigitado;
    private Clube clubeSelecionado;
    private List<Clube> clubeTabelas;
    private final DaoClube clubedao;

    public Clube getClubeDigitado() {
        return clubeDigitado;
    }

    public void setClubeDigitado(Clube clubeDigitado) {
        Clube oldclubeDigitado = this.clubeDigitado;
        this.clubeDigitado = clubeDigitado;
        propertyChangeSupport.firePropertyChange
        ("clubeDigitado",oldclubeDigitado,clubeDigitado); 
    }

    public Clube getClubeSelecionado() {
        return clubeSelecionado;
    }

    public void setclubeSelecionado(Clube clubeSelecionado) {
        if(this.clubeSelecionado !=null){
            setClubeDigitado(clubeSelecionado);
        }
        
        this.clubeSelecionado = clubeSelecionado;
    }

    public List<Clube> getclubeTabelas() {
        return clubeTabelas;
    }

    public void setClubeTabelas(List<Clube> clubeTabelas) {
        this.clubeTabelas = clubeTabelas;
    }
    
    public ClubeController(){
        clubedao = new DaoClube();
        clubeTabelas = ObservableCollections.observableList(new ArrayList<Clube>());
        novo();
        pesquisar();     
    }
    
    public void salvar(){
        clubedao.salvarAtualizar(clubeDigitado);
        novo();
        pesquisar();
    }
    
    public void excluir(){
        clubedao.excluir(clubeDigitado);
        novo();
        pesquisar();
    }
    
    public void novo() {
        setClubeDigitado(new Clube());
        
    }

    public void pesquisar() {
        clubeTabelas.clear();
        clubeTabelas.addAll(clubedao.pesquisar(clubeDigitado));
    }
    
    public void addPropertyChangeListener(PropertyChangeListener e) {
        propertyChangeSupport.addPropertyChangeListener(e);
    }
    
    public void removePropertyChangeListener(PropertyChangeListener e) {
        propertyChangeSupport.removePropertyChangeListener(e);
    }
    
}
