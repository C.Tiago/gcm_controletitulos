/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import domain.Clube;
import java.util.List;
import java.util.Objects;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author PabloSoares
 */
public class DaoClube {
    
    public void salvarAtualizar (Clube clube){
        EntityManager em = Conexao.getEntityManager();
        em.getTransaction().begin();
        if (clube.getId()!=null){
        em.merge(clube);
        }
        em.persist(clube);
        em.getTransaction().commit();
        em.close();
       
   }
   
    public void excluir (Clube clube){
        EntityManager em = Conexao.getEntityManager();
        em.getTransaction().begin();
        clube = em.merge(clube);
        em.remove(clube);
        em.getTransaction().commit();
        em.close();   
   }
   
    public List<Clube> pesquisar(Clube clube){
        EntityManager em = Conexao.getEntityManager();
        StringBuilder sql = new StringBuilder("from Clube c where 1=1");
      
        if(clube.getId()!=null){
            sql.append("and c.id= :id ");
        }
        if(clube.getNome()!=null && !clube.getNome().equals("")){
            sql.append("and c.nome like :nome ");
        }
        if(clube.getApelido()!=null && !clube.getApelido().equals("")){
            sql.append("and c.apelido like :apelido ");
        }
        if(clube.getGenero()!=null && !clube.getGenero().equals("")){
            sql.append("and c.genero like :genero ");
        }
        if(clube.getEstado()!=null && !clube.getEstado().equals("")){
            sql.append("and c.estado like :estado ");
        }
        Query query = em.createQuery(sql.toString());
     
        if(clube.getId()!=null){
            query.setParameter("id", clube.getId());
        } 
        if(clube.getNome()!=null && !clube.getNome().equals("")){
            query.setParameter("nome","%" + clube.getNome());
        }
        if(clube.getApelido()!=null && !clube.getApelido().equals("")){
            query.setParameter("apelido","%" + clube.getApelido());
        }
        if(clube.getGenero()!=null && !clube.getGenero().equals("")){
            query.setParameter("genero","%" + clube.getGenero());
        }
        if(clube.getEstado()!=null && !clube.getEstado().equals("")){
            query.setParameter("estado","%" + clube.getEstado());
        }
        return query.getResultList();
    }
    
      public List<String> findClubes() {
        EntityManager em = Conexao.getEntityManager();
        Query query = em.createQuery("SELECT c.nome FROM Clube c");
        return (List<String>) query.getResultList();
    }    
}



