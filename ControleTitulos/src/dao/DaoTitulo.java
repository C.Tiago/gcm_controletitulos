/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import domain.Titulo;

/**
 *
 * @author Gerim
 */
public class DaoTitulo {
    
    public void salvarAtualizar (Titulo titulo){
        EntityManager em = Conexao.getEntityManager();
        em.getTransaction().begin();
        if (titulo.getIdTitulo()!=null){
            titulo = em.merge(titulo);
        }
        em.persist(titulo);
        em.getTransaction().commit();
        em.close();
       
   }
   
    public void excluir (Titulo titulo){
        EntityManager em = Conexao.getEntityManager();
        em.getTransaction().begin();
        titulo = em.merge(titulo);
        em.remove(titulo);
        em.getTransaction().commit();
        em.close();   
}
   
    public List<Titulo> pesquisar(Titulo titulo){
        EntityManager em = Conexao.getEntityManager();
        StringBuilder sql = new StringBuilder("from Titulo t where 1=1");
      
        if(titulo.getIdTitulo()!=null){
            sql.append("and t.idTitulo= :id ");
        }
        if(titulo.getNome()!=null && !titulo.getNome().equals("")){
            sql.append("and t.nome like :nome ");
        }
          if(titulo.getTipo()!=null && !titulo.getTipo().equals("")){
            sql.append("and t.tipo like :tipo ");
        }
          if(titulo.getSerie()!=null && !titulo.getSerie().equals("")){
            sql.append("and t.serie like :serie ");
        }
            if(titulo.getAno()!=null && !titulo.getAno().equals("")){
            sql.append("and t.ano like :ano ");
        }
        Query query = em.createQuery(sql.toString());

        if(titulo.getIdTitulo()!=null){
            query.setParameter("id", titulo.getIdTitulo());
        }
        if(titulo.getNome()!=null && !titulo.getNome().equals("")){
            query.setParameter("nome","%" + titulo.getNome());
        }
        if(titulo.getTipo()!=null && !titulo.getTipo().equals("")){
            query.setParameter("tipo","%" + titulo.getTipo());
        }
        if(titulo.getSerie()!=null && !titulo.getSerie().equals("")){
            query.setParameter("serie","%" + titulo.getSerie());
        }
        if(titulo.getAno()!=null && !titulo.getAno().equals("")){
            query.setParameter("ano","%" + titulo.getAno());
        }
        
        if(titulo.getClube()!=null){
            query.setParameter("clube","%" + titulo.getClube());
        }
        return query.getResultList();
    }
}

